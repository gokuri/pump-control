
//Set the Pins
const int pumpPin =  8;// the number of the pump pin
const int ledPin =  12;// the number of the LED pin

//Constants
const unsigned long pumpWaitPeriod = 30000;//14340000;
const unsigned long pumpRunPeriod = 10000;//6000;
const unsigned long ledOnPeriod; // Not used since time interval is fixed
const unsigned long  ledOffPeriod; //Not used since time interval is fixed

// Timers for each process
unsigned long currentTime = 0;
unsigned long pumpTimer = 0;
unsigned long ledTimer =0;
unsigned long Timer=0;

// inital conditions of each process on system start
unsigned long pumpInterval = 10000;  //14340000; this is the initial pump interval after just starting the system
unsigned long ledInterval = 30000;//43200000; 
int pumpState = LOW;             // pumpState used to set the pump
int ledState = LOW;             // ledState used to set the LED


void setup() {
  pinMode(pumpPin, OUTPUT);
  pinMode(ledPin, OUTPUT);
  Serial.begin(9600);
  Serial.print("Pump connected to pin: ");
  Serial.println(pumpPin);
  Serial.print("LED connected to pin: ");
  Serial.println(ledPin);
  delay(500);
  Serial.println("...all systems are a go");
  Serial.print("pump will activate in: ");
  Serial.print(pumpInterval/1000);
  Serial.println("  seconds");
  Serial.print("LED Status: ");
  Serial.print(ledInterval/1000);
  Serial.println("  seconds");
}

void loop() {
   currentTime = millis();
   elapsedTime(currentTime);
   Pump(currentTime);
   LED(currentTime);
   
}

/*===Pump Process===*/ 
void Pump(unsigned long a){
  
  if(a >= pumpTimer + pumpInterval){
    pumpTimer +=  pumpInterval;

    
    if (pumpState == LOW) {
      Serial.println("Activating The pump");
      pumpState = HIGH;
      pumpInterval = pumpRunPeriod;
    } 
    else if(pumpState == HIGH) {
      Serial.println("Deactivating The pump");
      pumpState = LOW;
      pumpInterval = pumpWaitPeriod;
    }
    digitalWrite(pumpPin, pumpState);
    Serial.print("waiting: ");
    Serial.print(pumpInterval/1000);
    Serial.println("  seconds");
   }
 
 
 }


/*===LED Process===*/ 
void LED(unsigned long a){
  if(a >= ledTimer + ledInterval){
    ledTimer +=  ledInterval;

    
    if (ledState == LOW) {
      Serial.println("LED: ON");
      ledState = HIGH;
    } 
    else if(ledState == HIGH) {
      Serial.println("LED: OFF");
      ledState = LOW;
    }
    digitalWrite(ledPin, ledState);
    Serial.print("waiting: ");
    Serial.print(ledInterval/1000);
    Serial.println("  seconds");
   }
 }

/*===Get the Elapsed Time===*/ 
void elapsedTime(unsigned long a){
  
  if(a >= Timer + 1000){
  Timer += 1000;
  Serial.println(a/1000);
    }
  }


/*===Emergency stop function===*/ 
void stopPump(){
  digitalWrite(pumpPin, LOW);
}
